import numpy as np
import pandas as pd
from bokeh.io import output_notebook, show
from bokeh.io import output_file, reset_output
from bokeh.plotting import figure
from bokeh.transform import factor_cmap, factor_mark, linear_cmap, transform
from bokeh.palettes import Spectral, Colorblind, Viridis256, Category10
from bokeh.models import ColumnDataSource, HoverTool, Range1d, BoxAnnotation


class Cost:
    def __init__(self, name=str(None), monthly_fixed=0., monthly_percent=0., yearly_fixed=0., yearly_percent=0., yearly_percent_dividends=0.):
        self.name = name
        self.monthly_fixed = monthly_fixed
        self.yearly_fixed = yearly_fixed
        self.monthly_percent = monthly_percent
        self.yearly_percent = yearly_percent
        self.yearly_percent_dividends = yearly_percent_dividends
    
    # https://stackoverflow.com/a/141777/5599687
    @classmethod
    def from_set_of_Costs(cls, set_of_Costs):
        return cls(
            monthly_fixed   = np.array([c.monthly_fixed   for c in set_of_Costs]).sum(),
            monthly_percent = np.array([c.monthly_percent for c in set_of_Costs]).sum(),
            yearly_fixed    = np.array([c.yearly_fixed    for c in set_of_Costs]).sum(),
            yearly_percent  = np.array([c.yearly_percent  for c in set_of_Costs]).sum(),
            yearly_percent_dividends  = np.array([c.yearly_percent_dividends  for c in set_of_Costs]).sum()
        )


            
            
class Dossier:
    def __init__(self, source_history, costs={}):
        self.source_history = source_history
        self.costs = Cost.from_set_of_Costs(costs)
        self.df_history = self.get_quote_history()
        
        
    def plan_investment(self, one_time_events={}, periodic_contributions=[]):
        self.stocks = 0
        self.last_date = None
        self.current_quote = None
        self.money_invested = 0
        self.one_time_events = one_time_events
        self.periodic_contributions = periodic_contributions
        self.df = None
        self.df = self.get_quote_history() #self.df_history.copy(deep=True)
        
        
    def get_quote_history(self):
        df = pd.read_csv(self.source_history)
        df = df.drop('Fondo', axis=1)
        df.columns = ['text_date', 'quote']
        df['date'] = pd.to_datetime(df['text_date'], format="%d-%m-%Y")
        df.index = df.index[::-1]
        df = df.iloc[::-1]
        return df
    
    def buy(self, amount):
        self.transaction(amount)
        self.money_invested += amount

    def transaction(self, amount):
        self.stocks += amount/self.current_quote

    def run(self):
        self.df['stocks'] = pd.Series(index=self.df.index, dtype='float64')
        self.df['stocks_value'] = pd.Series(index=self.df.index, dtype='float64')
        self.df['money_invested'] = pd.Series(index=self.df.index, dtype='float64')
        first_time = min(self.one_time_events.keys())
        for index, row in self.df.iterrows(): 
            current_row = self.df.iloc[index]
            last_row = self.df.iloc[index-1]
            date = row['date']
            self.current_quote = row['quote'] 
            if date >= first_time:
                for otd, ote in self.one_time_events.items():
                    if (otd > self.last_date and otd <= date):
                        if ote[0] == 'buy':
                            self.buy(ote[1])
                if (self.last_date is not None) and (date.month != self.last_date.month): # new month
                    self.transaction(-(self.costs.monthly_fixed + self.costs.monthly_percent * self.stocks * self.current_quote))
                    for contr in self.periodic_contributions:
                        if contr[0] == 'monthly':
                            self.buy(contr[1])
                if (self.last_date is not None) and (date.year != self.last_date.year): # new year
                    self.transaction(-(self.costs.yearly_fixed + self.costs.yearly_percent * self.stocks * self.current_quote
                                      + self.costs.yearly_percent_dividends * (
                                          self.stocks * self.current_quote - last_row['stocks_value'])))
                    for contr in self.periodic_contributions:
                        if contr[0] == 'yearly':
                            self.buy(contr[1])
                self.df.loc[index, 'stocks'] = self.stocks
                self.df.loc[index, 'stocks_value'] = self.current_quote * self.stocks
                self.df.loc[index, 'money_invested'] = self.money_invested
            self.last_date = date
        self.df['profit'] = self.df['stocks_value'] - self.df['money_invested']

                
    def get_value(self):
        return self.stocks * self.current_quote
        
        
    def plot(self):
        p = figure(width=900, plot_height=500, x_axis_type="datetime")
        #p.line('date', 'quote', legend_label='quote', source=dossier_bilanciato.df)
        p.line('date', 'money_invested', legend_label='invested money', line_width=2, color='grey', source=self.df)
        p.line('date', 'stocks_value', legend_label='value of invested money', line_width=2, color='blue', source=self.df)
        p.line('date', 'stocks', legend_label='number of stocks', line_width=2, color='green', source=self.df)
        p.line('date', 'stocks', legend_label='number of stocks', line_width=2, color='green', source=self.df)
        p.line('date', 'profit', legend_label='profits', line_width=2, color='red', source=self.df)
        output_notebook()
        show(p)

class CompareFunds:
    def __init__(self, funds, costs, strategy, n_starts=5):
        """
        Compare different investment funds, providing:
        - `funds`: a dictionary of the form 
        ```
        {'fund_name': 'x/y.csv', # a .csv file with columns ["fund name", date", "quote"],}
        ```
        - `costs`: a dictionary of the form
        ```
        {
            (name='account', yearly_fixed=110.),
            Cost(name='taxes', yearly_percent=0.002),        
        }
        ```
        - `strategy`: a dictionary of the form
        ```
        {
            initial_contribution=1000.,
            periodic_contributions=[('monthly', 300.)]
        }
        - `n_starts`: the number of start dates to try
        - `n_ends`: the number of end dates to try
        ```
        """
        self.funds = funds
        self.costs = costs
        self.strategy = strategy
        self.df_results = pd.DataFrame()
        self.n_starts = n_starts
        
    def run(self, verbose=False):
        for name, fund_url in self.funds.items():
            if verbose:
                print(name)
            dossier = Dossier(
                source_history=fund_url, 
                costs=self.costs
            )
            birthdate = dossier.df_history['date'].min()
            start_dates = [birthdate+pd.Timedelta('10D')+n*pd.Timedelta(15*365./self.n_starts,'D') 
                           for n in range(self.n_starts)]
            for start_date in [dd for dd in start_dates if dd<dossier.df_history['date'].max()][:]:
                if verbose:
                    print('  initial date: ', start_date)
                dossier.plan_investment(
                    one_time_events={ 
                        pd.to_datetime(start_date): ('buy', self.strategy['initial_contribution']),
                    }, 
                    periodic_contributions=self.strategy['periodic_contributions']
                )
                dossier.run()
                 
                deposit_times = [n*pd.Timedelta('365D') for n in range(15)]
                for time_of_deposit in [dd for dd in deposit_times if start_date + dd <= dossier.df['date'].max()]:
                    df_after_deposit = dossier.df.loc[
                        dossier.df['date'] >= start_date+time_of_deposit
                    ].loc[
                        dossier.df['date'] <= start_date+time_of_deposit+pd.Timedelta('10D')
                    ]
                    profit = df_after_deposit['profit'].mean()
                    self.df_results = self.df_results.append({
                        'fund_name': name, 
                        'birthdate': birthdate,
                        'start_date': start_date, 
                        'text_start': str(start_date),
                        'duration': time_of_deposit, 
                        'end_date': start_date + time_of_deposit, 
                        'text_end': str(start_date + time_of_deposit),
                        'gross_profit': profit
                    }, ignore_index=True)
        self.df_results['minimum_net_profit'] = self.df_results['gross_profit']*(1-0.26)
        self.df_results['years'] = pd.Series([((t.days/365.)) for t in self.df_results['duration']])

    def plot(self):
        grouped_df = self.df_results.groupby(['fund_name','years'], as_index=False).mean()
        source_data = ColumnDataSource(data=grouped_df)#.to_dict(orient='list'))
        group = grouped_df.groupby('fund_name')
        i = 4
        name_cmap = factor_cmap('fund_name', palette=Colorblind[3+i][i:], 
                                factors=sorted([str(y) for y in self.df_results.fund_name.unique()]))
        name_mmap = factor_mark('fund_name', markers=['circle', 'triangle', 'square'], 
                                factors=sorted([str(y) for y in self.df_results.fund_name.unique()]))
        p = figure(width=900, plot_height=500, tools=["pan,wheel_zoom,box_zoom,reset"])
        grouped_df_stat = self.df_results.groupby(['fund_name','years'], as_index=False).agg(['mean', 'max', 'min'])
        for index, fn in enumerate(sorted(self.df_results.fund_name.unique())):
            source_error = ColumnDataSource(data=dict(
                base=grouped_df_stat[('minimum_net_profit', 'mean')].loc[fn].index,
                mean=grouped_df_stat[('minimum_net_profit', 'mean')].loc[fn],
                lower=grouped_df_stat[('minimum_net_profit', 'min')].loc[fn],
                upper=grouped_df_stat[('minimum_net_profit', 'max')].loc[fn]
            ))
            color_fund = name_cmap['transform'].palette[index]
            
            p.varea(y1='lower', y2='upper', x='base', alpha=0.5, color=color_fund, source=source_error)
            p.line(x='base', y='mean', legend_label=fn, line_width=3, color=color_fund, source=source_error)
        p.add_layout(BoxAnnotation(top=0, fill_alpha=0.1, fill_color='black'))
        p.legend.location = "top_left"
        p.xaxis.axis_label = "duration"
        p.yaxis.axis_label = "profit"
        p.x_range=Range1d(0, 10.5)
        output_notebook()        
        #output_file('funds_results.html')
        show(p)
        #reset_output()
